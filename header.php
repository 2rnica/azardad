<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title();?></title>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />	
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats please -->

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
 
   
 	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>
	<?php wp_head();?>
 
 
  	<script type="text/javascript">
   		jQuery(document).ready(function() {
			jQuery().pgcarousel();
			jQuery().azslider();
			jQuery().pgmenu();
			jQuery().azspecial();	 
			jQuery().azset();
			jQuery(document).foundation();
   		});
    </script>


	</head>
<body>
	<div class="off-canvas-wrap" data-offcanvas>
  		<div class="inner-wrap">
    		<nav class="tab-bar show-for-small-only">
      			<section class="left-small">
        			<a class="left-off-canvas-toggle menu-icon"  href="#"><span></span></a>
      			</section>
      			<section class="middle tab-bar-section">
       				<h1 class="title">آذرداد</h1>
      			</section>
      			<section class="right-small">
        			<a class="right-off-canvas-toggle menu-icon" href="#"><span></span></a>
      			</section>
    		</nav>
    		<aside class="left-off-canvas-menu">
      			<ul class="off-canvas-list">
        			<li><label>منوی اصلی</label></li>
        			<?php wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' =>'section', 'menu_class' =>'o' ) ); ?>
      			</ul>
    		</aside>
    		<aside class="right-off-canvas-menu">
      			<ul class="off-canvas-list">
        			<li><label>منوی فرعی</label></li>
            		<?php wp_list_categories('orderby=name&exclude=&title_li='); ?>
      			</ul>
    		</aside>