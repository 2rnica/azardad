		<footer class="row footerrow" >
            <div class="row footerrow2">
            	<div class="large-3 medium-3 small-3 hide-for-small-only columns">
                	 <?php dynamic_sidebar('Footer 01'); ?>
				</div>
            	<div class="large-3 medium-3 small-6 columns">
                	 <?php dynamic_sidebar('Footer 02'); ?>
				</div>
            	<div class="large-3 medium-3 small-6 columns">
                	 <?php dynamic_sidebar('Footer 03'); ?>
				</div>
            	<div class="large-3 medium-3 small-3 hide-for-small-only columns">
                	 <?php dynamic_sidebar('Footer 04'); ?>
				</div>
			</div>
            <div class="row  footerrow3">
            	<div class="large-6 medium-6 small-6 columns" style="font-size:0.8rem">
                	 کلیه حقوق این سایت متعلق به <?php bloginfo("name"); ?> میباشد
				</div>
            	<div class="large-6 medium-6 small-6 left columns">
                	 <?php if(function_exists('cn_social_icon') ) echo cn_social_icon(); ?>
				</div>
           </div>
		</footer>
		<a class="exit-off-canvas"></a>
  </div>
</div>
<?php wp_footer(); ?>
</body>
</html>