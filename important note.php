<?php
// The Query
$query = new WP_Query( 'product_tag=special' );

echo '<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">';
// The Loop
while( $query->have_posts() ):
	$query->next_post();
	echo '<li>';
	echo '<h3><a href="'. get_permalink( $query->post->ID ) . '">' . get_the_title( $query->post->ID ) . '</a></h3>';
	echo get_the_post_thumbnail( $query->post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
$product = new WC_Product( $query->post->ID );
$price = $product->get_regular_price( );
$price = $product->get_price( );
echo $price.get_woocommerce_currency_symbol();
echo "<a href=".$product->add_to_cart_url( ).'>tst</a>';

//////////////////////////
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
		esc_attr( $product->product_type ),
		esc_html( $product->add_to_cart_text() )
	),
$product );
//////////////////////
echo "<a href='" . $product->add_to_cart_url() ."'>add to cart</a>";
/////////////////////
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo $price_html; ?></span>
<?php endif; 
////////////////////////////
	
wc_get_template( 'single-product/add-to-cart/simple.php' );
//
wc_get_template( 'single-product/add-to-cart/grouped.php', array(
			'grouped_product'    => $product,
			'grouped_products'   => $product->get_children(),
			'quantites_required' => false
		) );
///////////////////////////
$add_to_cart = do_shortcode('[add_to_cart_url id="'.$query->post->ID.'"]'); ?>
<a href="<?php echo $add_to_cart ?>"><img src="'. get_template_directory_uri() . '/img/small-cart.png" />Buy Now</a>
<?php
echo do_shortcode('[product id="153"]');
echo do_shortcode('[add_to_cart id="153"]');
//////////////////////
	echo '</li>';
endwhile;
echo '</ul>';
// Restore original Query & Post Data
wp_reset_query();
wp_reset_postdata();
?>