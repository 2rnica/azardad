<?php 
	get_header(); 
?>
<div class="row">
	<div class="row">
    	<div class="row BNazaninText">
        	<div class="large-3 medium-4 small-4 right columns">
                <img style="margin-right: 10px !important;" src="<?php echo bloginfo('template_url').'/img/nlogo.png' ?>">
			</div>
        	<div class="large-3 medium-4 small-8 left columns">
        		<div class="large-3 medium-3 small-3 right columns">
            		<img src="<?php echo bloginfo('template_url').'/img/phone.png' ?>" />
				</div>
        		<div class="large-9 medium-9 small-9 text-left left columns">
                	021 - 66 99 88 77<br /><br />021 - 66 99 88 77
				</div>
			</div>
		</div>
		<div class="row">
        	<div class="large-9 medium-9 hide-for-small-only right columns">
            	<!-- منوی اصلی -->
                <div id="main-menu">
                	<?php wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' =>'section', 
					'menu_class' =>'topnav' ) ); ?>
					<div class="clear"></div>
				</div>
			</div>
        	<div class="large-3 medium-3 hide-for-small-only hide-for-medium-only text-left left columns">
            	<?php  echo jdate ('امروز : l  j F   Y ') ; ?>
            	<br />
                <?php get_search_form(  ); ?>
			</div>
		</div>
	</div>
	<div class="row">
    	<!-- اسلایدر اصلی -->
		<div class=" large-12 medium-12 small-12 large-centered medium-centered small-centered columns">
			<?php if (function_exists('layerslider')) { layerslider(1);} ?>
		</div>
	</div>
            <br /><br />
            <!-- کراسول اسلایدر -->
	<div class="row mborder hide-for-small-only hide-for-medium-only">
    	<span class="control prev"><img src="<?php echo bloginfo('template_url').'/img/leftarrow.png' ?>"></span>
		<div class="mask large-12 medium-12 small-12 large-centered hide-for-small-only hide-for-medium-only columns">
			<ul class="images">
            	<?php
					query_posts('tag=sp');
					while (have_posts()) : the_post();
    					foreach((get_the_category()) as $category){ 
							$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
				?>
   				<li>
     				<img  src="<?php echo $url ?>" />
                                        <br />
                    <?php echo the_title(); ?>

   				</li>                
				<?php
    					}
					endwhile; 
					wp_reset_query();
				?>
			</ul>
		</div>
		<span class="control next"><img src="<?php echo bloginfo('template_url').'/img/rightarrow.png' ?>"></span>
	</div>
	<ul class="triggers">
    <?php
		query_posts('tag=sp');
		while (have_posts()) : the_post();
    		foreach((get_the_category()) as $category){ 
   				echo "<li></li>";                
    		}
		endwhile; 
		wp_reset_query();
	?>
	</ul>

    	
	<div class="row vspace">
        <div class=" large-3 medium-3 small-0 hide-for-small-only  blockheader2 columns">
        	<div class="row mborder2">
            	<!-- دسته بندی ها -->
            	<div class="large-12 medium-12 small-12 columns">
        			<h3 class="blockheadertext"><?php echo __( 'Categories','AzrDad' ) ?></h3>
            		<div id="navigation" class="setradius">
        				<?php //wp_list_categories('orderby=name&exclude=&title_li='); ?>
                        <?php wp_list_categories( 'taxonomy=product_cat&pad_counts=1&title_li=' ); ?>
            		</div>
                </div>
            </div>
            <!-- عضویت در سایت -->
            <div class="row">
				<div class="large-12 medium-12 small-12  signupback columns">
                	<h4 class="signuph text-center"><?php echo __( 'SignUp','AzrDad' ) ?></h4>
                	<?php custom_registration_function() ?>
                </div>
            </div>
            <!-- آمار سایت -->
            <div class="row">
				<div class="large-12 medium-12 small-12  countborder columns">
                	<h4 class="counth"><?php echo __( 'Statistics','AzrDad' ) ?></h4>
                    <?php dynamic_sidebar('Counter Widget'); ?>
                </div>
            </div>
        </div>
		<div class=" large-9 medium-7 small-12 hspace  columns">
									<?php ShowProductspage(); ?>
							
  							
        </div>
	</div>
</div>
            <!-- عضویت در سایت -->
            <div class="row">
				<div class="large-12 medium-12 small-12 show-for-small-only signupback columns">
                	<h4 class="signuph text-center"><?php echo __( 'SignUp','AzrDad' ) ?></h4>
                	<?php custom_registration_function() ?>
                </div>
            </div>

<!-- Footer -->
<?php get_footer(); ?>
</body>
</html>