<?php
function pg_load_text_domains(){
    $lt_text_domain = load_theme_textdomain("AzrDad", get_template_directory() . '/langs');
}
add_action('after_setup_theme', 'pg_load_text_domains');
/////////////////////////////////////////////////////////////////////
function joints_scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {
   wp_enqueue_media();
    // register foundation stylesheet
    wp_register_style('azar-style',get_stylesheet_directory_uri() . '/css/azcs.css');
	wp_enqueue_style('azar-style');
    //adding scripts file in the footer
    wp_enqueue_script( 'azarscrip', get_template_directory_uri() . '/js/azarscrip.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'foundation.min', get_template_directory_uri() . '/js/foundation.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation/foundation.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'foundation.topbar', get_template_directory_uri() . '/js/foundation/foundation.topbar.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'foundation.reveal', get_template_directory_uri() . '/js/foundation/foundation.reveal.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'foundation.tab', get_template_directory_uri() . '/js/foundation/foundation.tab.js', array( 'jquery' ), '', true );    
	wp_enqueue_script( 'foundation.offcanvas', get_template_directory_uri() . '/js/foundation/foundation.offcanvas.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', array( 'jquery' ), '', true );

  }
}
    add_action('wp_enqueue_scripts', 'joints_scripts_and_styles', 10000);
///////////////////////////////////////////////////////////////////////////////////////
if ( function_exists('add_theme_support') ) {add_theme_support('post-thumbnails');}

if ( function_exists('register_sidebar') ){
 	register_sidebar( array(
		'name'          => __( 'Counter Widget','AzrDad' ),
		'id'            => 'Counter-Widget',
		'description'  => __( 'Widgets in this area will be shown on the right-hand side.', 'AzrDad'  ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Start Words Widget', 'AzrDad' ),
		'id'            => 'Start-Words-Widget',
		'description'  => __( 'Widgets in this area will be shown on Start Words Part.', 'AzrDad' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
 	register_sidebar( array(
		'name'          => __( 'Footer 01', 'AzrDad' ),
		'id'            => 'Footer01',
		'description'  => __( 'Widgets in this area will be shown on the footer', 'AzrDad' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
 	register_sidebar( array(
		'name'          => __( 'Footer 02', 'AzrDad' ),
		'id'            => 'Footer02',
		'description'  => __( 'Widgets in this area will be shown on the footer.', 'AzrDad' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );	
 	register_sidebar( array(
		'name'          => __( 'Footer 03', 'AzrDad' ),
		'id'            => 'Footer03',
		'description'  => __( 'Widgets in this area will be shown on the footer.', 'AzrDad' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );	
 	register_sidebar( array(
		'name'          => __( 'Footer 04', 'AzrDad' ),
		'id'            => 'footer04',
		'description'  => __( 'Widgets in this area will be shown on the footer.', 'AzrDad' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );	
	
	
	
	}
function register_my_menus() {
	register_nav_menus(
	array('top-menu' => __( 'top menu','AzrDad' ))
	);
}

add_action( 'init', 'register_my_menus' );
//
add_theme_support( 'woocommerce' );	
//افزودن تنظیمات هدر	
add_theme_support( 'custom-header' );
//افزودن شمارشگر صفحات	
function mw_pagination($pages = '', $range = 2){
	$showitems = ($range * 2)+1;
	global $paged;
	if(empty($paged)){
		$paged = 1;
	}
	if($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages){
			$pages = 1;
		}
	}
	if(1 != $pages){
    	echo  '<div class="pagination-centered">';
		echo "<ul class='pagination'>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages){
			 echo "<li class='arrow'><a href='".get_pagenum_link(1)."'>&laquo;</a></li>";
		}
		if($paged > 1 && $showitems < $pages){
			 echo "<li class='arrow'><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";
		}
		for ($i=1; $i <= $pages; $i++){
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? "<li class='current'>".$i."</li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
			}
		}
		if ($paged < $pages && $showitems < $pages){
			 echo "<li class='arrow'><a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";
		}
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
			echo "<li class='arrow'><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
			echo "</ul>\n";
		}
	}

	
  echo  "</div>";
}
function new_excerpt_more( $more ) {


    return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">ادامه مطلب</a>';

}
add_filter( 'excerpt_more', 'new_excerpt_more' );
function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
////////////////////////////////////////

	
//////////////////////////////////////////////////////////
//ساخت فرم لاگین
function registration_form( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio ) {
    echo '
    <style>
    div {
        margin-bottom:2px;
    }
     
    input{
        margin-bottom:4px;
    }
    </style>
    ';
 	echo '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post"> 
 			<div class="row"> 
				<div class="large-12 medium-12 small-12 columns"> 
					<div class="row"> 
						<div class="small-4 columns"> 
							<label for="username" class="right regformlabel">'.__( "username","AzrDad" ).'</label> 
						</div> 
						<div class="small-8 columns"> 
							<input type="text" name="username" value="' . 
							( isset( $_POST['username'] ) ? $username : null ) . '"> 
						</div> 
					</div>
					<div class="row"> 
						<div class="small-4 columns"> 
							<label for="password" class="right regformlabel">'.__( "password","AzrDad" ).'</label> 
						</div> 
						<div class="small-8 columns"> 
							<input type="password" name="password" value="' . 
							( isset( $_POST['password'] ) ? $password : null ) . '"> 
						</div> 
					</div>	
					<div class="row"> 
						<div class="small-4 columns"> 
							<label for="email" class="right regformlabel">'.__( "email","AzrDad" ).'</label> 
						</div> 
						<div class="small-8 columns"> 
							<input type="text" name="email" value="' . 
							( isset( $_POST['email']) ? $email : null ) . '"> 
						</div> 
					</div>
					<div class="row"> 
						<div class="small-12  columns">
							<input class="regbtn button tiny" type="submit" name="submit" value="'.__( "Register","AzrDad" ).'"/> 
						</div> 
					</div>					
				</div> 
			</div> 
		</form>';
  	
}
function registration_validation( $username, $password, $email )  {
	global $reg_errors;
	$reg_errors = new WP_Error;
	if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
    	$reg_errors->add('field', __( "Required form field is missing","AzrDad" ));
	}
	if ( 4 > strlen( $username ) ) {
    	$reg_errors->add( 'username_length',__( "Username too short. At least 4 characters is required","AzrDad" ) );
	}
	if ( username_exists( $username ) )
    	$reg_errors->add('user_name',__( "Sorry, that username already exists!","AzrDad" ));	

	if ( ! validate_username( $username ) ) {
    	$reg_errors->add( 'username_invalid',__( "Sorry, the username you entered is not valid","AzrDad" )   );
	}
	if ( 5 > strlen( $password ) ) {
        $reg_errors->add( 'password',__( "Password length must be greater than 5","AzrDad" )   );
    }
	if ( !is_email( $email ) ) {
    	$reg_errors->add( 'email_invalid',__( "Email is not valid","AzrDad" ));
	}
	if ( email_exists( $email ) ) {
    	$reg_errors->add( 'email',__( "Email Already in use","AzrDad" ) );
	}
	if ( is_wp_error( $reg_errors ) ) {
    	foreach ( $reg_errors->get_error_messages() as $error ) {
        	echo '<div>';
        	echo '<strong>'.__( "ERROR","AzrDad" ) .'</strong>:';
        	echo $error . '<br/>';
        	echo '</div>'; 
    	}
 
	}
}
function complete_registration() {
    global $reg_errors, $username, $password, $email;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
        'user_login'    =>   $username,
        'user_email'    =>   $email,
        'user_pass'     =>   $password
        );
        $user = wp_insert_user( $userdata );
        echo __( 'Registration complete. Goto ',"AzrDad" ). 
		'<a href="' . get_site_url() . '/wp-login.php">'.__( ' Login form ',"AzrDad" ).'</a>.';  
    }
}

function custom_registration_function() {
    if ( isset($_POST['submit'] ) ) {
        registration_validation(
        $_POST['username'],
        $_POST['password'],
        $_POST['email']
        );
         
        // sanitize user form input
        global $username, $password, $email;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email
        );
    }
 
    registration_form(
        $username,
        $password,
        $email
        );
}
///////////////////////////////
function ShowProducts(){
	echo '<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-3">';
 	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); 
   	if(have_posts()) :                
  	 	while(have_posts()) : the_post()?>
        	<li>
  				<div class="post panel">
                <?php
                	if ( has_post_thumbnail() ) {
						the_post_thumbnail('thumbnail');
					} 
				?>
    				<h3>
            			<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
      						<?php the_title(); ?>
      					</a>
            		</h3>
                    
                <?php
					if (get_post_type(get_the_ID())=='product'){
						$product = new WC_Product( get_the_ID() );
						$price = $product->get_price( );
						echo "<p class='pprice text-left BNazaninTitr'>";
						echo $price;
						echo " ";
						echo get_woocommerce_currency_symbol();
						echo "</p>";
						$add_to_cart = do_shortcode('[add_to_cart_url id="'.get_the_ID().'"]'); 
				?>
						<a class="button tiny cbtn" href="<?php echo get_permalink( get_the_ID() ) ?>">
                        	
                       	</a>
						<a class="button tiny cbtn2" href="<?php echo $add_to_cart ?>">
                        	
                       	</a>
                        <?php
					}
					else{
						?>
                            				<div class="entry">
      					<?php the_excerpt(); ?>
      					<p class="postmetadata">
        					<?php _e('ارسال شده در&#58;'); 
         					the_category(', '); 
							echo "<br>";
       				 		_e('نویسنده:'); 
        	  				the_author();
							echo "<br>"; 
        					$posttags = get_the_tags();
							if ($posttags) {
								foreach($posttags as $tag) {
    								echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>'.',';
								}
							}?>
        					<br />
     				 	</p>
        				<?php 
        					edit_post_link('ویرایش', ' &#124; ', '');
  			 				comments_template(); 
						?>    								
					</div>

                        
                        <?php
						
					}
				?>
  				</div>
			</li>
  		<?php endwhile; ?>
  	<?php else : ?>
  		<div class="post" id="post-<?php the_id(); ?>">
    		<h2>
      			<?php _e('Not Found'); ?>
    		</h2>
  		</div>
 	<?php endif; 
	echo "</ul>";
	?>
      		<div class="navigation">
			<div id="trans">
				<?php mw_pagination(); ?>
			</div>
  		</div>
<?php
}
///////////////////////////////////////////////////////
///////////////////////////////
function ShowProductspage(){
	echo '<ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-1">';
 	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); 
   	if(have_posts()) :                
  	 	while(have_posts()) : the_post()?>
        	<li>
  				<div class="post panel">
                <?php
                	if ( has_post_thumbnail() ) {
						the_post_thumbnail('thumbnail');
					} 
				?>
    				<h3>
            			<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
      						<?php the_title(); ?>
      					</a>
            		</h3>
                    
                <?php
					if (get_post_type(get_the_ID())=='product'){
						$product = new WC_Product( get_the_ID() );
						$price = $product->get_price( );
						echo "<p class='pprice text-left BNazaninTitr'>";
						echo $price;
						echo " ";
						echo get_woocommerce_currency_symbol();
						echo "</p>";
						$add_to_cart = do_shortcode('[add_to_cart_url id="'.get_the_ID().'"]'); 
				?>
						<a class="button tiny cbtn" href="<?php echo get_permalink( get_the_ID() ) ?>">
                        	
                       	</a>
						<a class="button tiny cbtn2" href="<?php echo $add_to_cart ?>">
                        	
                       	</a>
                        <?php
					}
					else{
						?>
                            				<div class="entry">
      					<?php the_content(); ?>
      					<p class="postmetadata">
        					<?php _e('ارسال شده در&#58;'); 
         					the_category(', '); 
							echo "<br>";
       				 		_e('نویسنده:'); 
        	  				the_author();
							echo "<br>"; 
        					$posttags = get_the_tags();
							if ($posttags) {
								foreach($posttags as $tag) {
    								echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>'.',';
								}
							}?>
        					<br />
     				 	</p>
        				<?php 
        					edit_post_link('ویرایش', ' &#124; ', '');
  			 				comments_template(); 
						?>    								
					</div>

                        
                        <?php
						
					}
				?>
  				</div>
			</li>
  		<?php endwhile; ?>
  	<?php else : ?>
  		<div class="post" id="post-<?php the_id(); ?>">
    		<h2>
      			<?php _e('Not Found'); ?>
    		</h2>
  		</div>
 	<?php endif; 
	echo "</ul>";
	?>
      		<div class="navigation">
			<div id="trans">
				<?php mw_pagination(); ?>
			</div>
  		</div>
<?php
}
/////////////////////////
add_action( 'woocommerce_after_shop_loop_item', 'my_112757_add_product_link', 11 );

function my_112757_add_product_link() {
     global $product;

     echo '<a class="cbtn cbtn-shop-page" href="' . esc_url( get_permalink( $product->id ) ) . '"></a>';
}
//////////////////////////////////////////////////////////////////////////////
add_action('admin_menu', 'azd_remove_menu_elements', 102);

function azd_remove_menu_elements()
{
	remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page( 'plugins.php', 'plugin-editor.php' );

}
///////////////////////////////////////////////////////////////////////////////
function pagination_ampersand_bug_workaround( $link ) {
    return str_replace( '#038;', '&', $link );
}
add_filter( 'paginate_links', 'pagination_ampersand_bug_workaround' );

/**
 * Replace WooCommerce Default Pagination with WP-PageNavi Pagination
 *
 * @author WPSnacks.com
 * @link http://www.wpsnacks.com
 */
remove_action('woocommerce_pagination', 'woocommerce_pagination', 10);
function woocommerce_pagination() {
    wp_pagenavi();
}
add_action( 'woocommerce_pagination', 'woocommerce_pagination', 10);
?>