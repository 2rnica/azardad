// JavaScript Document




(function( $ ) {
$.fn.pgcarousel = function() {
var triggers = $('ul.triggers li');
var images = $('ul.images li');
var lastElem = triggers.length-1;
var mask = $('.mask ul.images');
var imgWidth = images.width();
var target;

triggers.first().addClass('selected');
//mask.css('width', (imgWidth+10)*(lastElem+1) +'px');

function sliderResponse(target) {
	
    mask.stop(true,false).animate({'left':'-'+((imgWidth*target)+(42.93*target))+'px'},300);
    triggers.removeClass('selected').eq(target).addClass('selected');
}

triggers.click(function() {
    if ( !$(this).hasClass('selected') ) {
        target = $(this).index();
        sliderResponse(target);
        resetTiming();
    }
});
$('.prev,.chprev').click(function() {
    target = $('ul.triggers li.selected').index();
    target === lastElem ? target = 0 : target = target+1;
    sliderResponse(target);
    resetTiming();
});
$('.next,.chnext').click(function() {
    target = $('ul.triggers li.selected').index();
    lastElem = triggers.length-1;
    target === 0 ? target = lastElem : target = target-1;
    sliderResponse(target);
    resetTiming();
});
function sliderTiming() {
    target = $('ul.triggers li.selected').index();
    target === lastElem ? target = 0 : target = target+1;
    sliderResponse(target);
}
var timingRun = setInterval(function() { sliderTiming(); },5000);
function resetTiming() {
    clearInterval(timingRun);
    timingRun = setInterval(function() { sliderTiming(); },5000);
}

};
////////////
$.fn.azslider = function() {
	var triggers = $('ul.ntriggers li');
var images = $('ul.nimages li');
var lastElem = triggers.length-1;
var target;

triggers.first().addClass('nactive');
images.hide().first().show();

function sliderResponse(target) {
    images.fadeOut(300).eq(target).fadeIn(300);
    triggers.removeClass('nactive').eq(target).addClass('nactive');
}

triggers.click(function() {
    if ( !$(this).hasClass('nactive') ) {
        target = $(this).index();
        sliderResponse(target);
        resetTiming();
    }
});

$('.nnext').click(function() {
    target = $('ul.ntriggers li.nactive').index();
    target === lastElem ? target = 0 : target = target+1;
    sliderResponse(target);
    resetTiming();
});
$('.nprev').click(function() {
    target = $('ul.ntriggers li.nactive').index();
    lastElem = triggers.length-1;
    target === 0 ? target = lastElem : target = target-1;
    sliderResponse(target);
    resetTiming();
});

function sliderTiming() {
    target = $('ul.ntriggers li.nactive').index();
    target === lastElem ? target = 0 : target = target+1;
    sliderResponse(target);
}

var timingRun = setInterval(function() { sliderTiming(); },5000);
function resetTiming() {
    clearInterval(timingRun);
    timingRun = setInterval(function() { sliderTiming(); },5000);
}
}
///////////////////////////////////////////////////////////////////////////
$.fn.pgmenu = function() {

   $('ul.topnav li > a').each(function(index, element) {
        var atext=$(this).html();
		var ahref=$(this).attr('href');
		var newout="<a href="+"'"+ahref+"'"+"><span>"+atext+"</span></a>";
		$(this).replaceWith( newout );
    });
	      var zIndexNumber = 300;
               
                $('ul.topnav li').each(function() {
                    $(this).css('zIndex', zIndexNumber);
                    zIndexNumber += 1;
                });
}
	
///////////////////////////////////////
$.fn.azspecial = function() {
	$("ul.specialproducts li:nth-child(4)").after("<hr class='spline'>");
	$("ul.specialproducts li:nth-child(8)").after("<hr class='spline'>");
	$("ul.specialproducts li:nth-child(12)").after("<hr class='spline'>");
}
///////////////////////////////
$.fn.azset = function() {
 	 $("li.page_item_has_children").addClass('has-dropdown');
	 $("li.menu-item-has-children").addClass('has-dropdown');
	 console.log("ok");
	 $("ul.children").addClass('dropdown');
	 $("ul.sub-menu").addClass('dropdown');
	 $("input.button-primary").addClass('radius button tiny ');

		$("li.pagenav>ul").addClass("right");
		var jj = $("li.pagenav").html()
		$("li.pagenav").replaceWith(jj)
		$('.countborder >.widget_countperday_widget>.widget-title').remove();
		
		if($.browser.chrome) {
			$('span.prev').removeClass('prev').addClass('chprev');
			$('span.next').removeClass('next').addClass('chnext');
		} 
		else if ($.browser.mozilla) {
			$('span.chprev').removeClass('chprev').addClass('prev');			
			$('span.chnext').removeClass('chnext').addClass('next');
		} 
		else if ($.browser.msie) {
		}
}
//////////////////////////////
}( jQuery ));
